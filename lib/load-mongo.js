let mongod

module.exports = async function (cb) {

  // Before running any tests
  before(async function () {
    if (process.argv.includes('--no-mongo')) {
      console.log(`
      Skipping Mongo Memory DB Mocking as flagged on CLI!
      `)
      return cb()
    }

    console.info(`
    Mocking MongoDB with Mongo Memory DB for faster tests and throw away data!
     - maybe slow at first spin, but subsequent runs are way faster
    `)
    const { MongoMemoryServer } = require('mongodb-memory-server')

    mongod = new MongoMemoryServer()

    process.env.MONGODB_URL = await mongod.getConnectionString()

    console.info(`
     ✔︎ Done spinning up Mongo Memory DB
    `)
    // const port = await mongod.getPort();
    // const dbPath = await mongod.getDbPath();
    // const dbName = await mongod.getDbName();

    cb() // continue

  })

  // After tests are complete
  after(async function () {
    // stop mongod manually
    if (mongod && mongod.getInstanceInfo()) await mongod.stop()
  })

}
