// make sure that we flag the environment as development
if(!process.env.NODE_ENV || !process.env.NODE_ENV.includes('test')) process.env.NODE_ENV = 'test'

if (process.argv.includes('--no-boot')) {
  console.log('Skipping Bootstrapping as flagged on CLI!')
  return
}

console.info('Bootstrapping test environment...')


require('./global')
require('./load-mongo')(()=>require('./load-sails')) // load mongo memory db then load sails


