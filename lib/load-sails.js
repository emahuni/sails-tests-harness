
/*
 * Sails application launcher.
 *
 */

// console.log('process argv', process.argv);
// console.log('process log level: ', process.argv[process.argv.indexOf('--log')+1]);
const logLevel = process.argv.indexOf('--log') >= 0 && process.argv[process.argv.indexOf('--log') + 1] || 'debug'
// console.debug('LogLevel: ', logLevel);


if (process.argv.includes('--no-sails')) {
  console.log('Skipping loading of Sails as flagged on CLI!')
  return
}


const Sails = require('sails').Sails

// Before running any tests, attempt to lift Sails
before(function (done) {
  // Hook tests will timeout in 60 seconds, adjust for more
  this.timeout(60000)


  console.info(`
     Loading SailsJs...
    `)

  // Attempt to lift sails with a custom test environment configuration
  Sails().load({
    log: {
      level:   logLevel,
      // level: 'debug',
      // level: 'verbose',
      // level: 'silly',
      inspect: false,
    },

    // the rest of the configs go to config/env/test.js and do the default test envrionment config

  }, (err, _sails) => {
    if (err) {
      return done(err)
    }



    console.info(`
     ✔︎ Done loading SailsJs
    `)

    global.sails = _sails
    global.log = sails.log
    global._ = require('chai-match-pattern').getLodashModule()
    //
    // const Barrels = require('barrels')
    // const barrels = new Barrels()
    // global.fixtures = barrels.data
    // barrels.populate(function(err) {
    //   console.warn(`An error happened during instantiation of Barrels fixtures: `, err)
    // });


    return done()
  })
})

// After tests are complete
after(done => {
  // Lower Sails (if it successfully lifted earlier)
  if (sails) {
    //-- NOTE: This is a workaround for sails.lower multiple callback calls...
    var _shutting_down = false

    function _shutdown(err) {
      if (!_shutting_down) {
        _shutting_down = true
        done && done(err)
      }
    }

    sails.lower(_shutdown)
  } else {
    // Otherwise just return
    return done()
  }
})
