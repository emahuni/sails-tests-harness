

global.chai = require('chai')
chai.use(require('chai-factories'))

chai.use(require('sinon-chai'))
chai.should()

global.factory = {
  define: chai.factory,
  // work around for non-unique factories
  create: (ref) => {
    return require('lodash/cloneDeep')(chai.create(ref))
  },
}



chai.use(require('dirty-chai'))
//
// const mocha = require('mocha')
// let { describe, it, context, test } = mocha
// Object.assign(global, { describe, it, context })



chai.use(require('chai-match-pattern'))


global.chalk = require('chalk')
global.faker = require('faker')
// global.requestPromise = require('request-promise-native')


// get nice logging and tracing output with log source line numbers and filename
global.contrace = require('contrace')({
  stackIndex:    1,
  // configure for Sails
  methods:       ['silly', 'verbose', 'info', 'debug', 'warn', 'error'],
  // showFile: false,
  showTimestamp: false,
  showLogType:   false,
})


const path = require('path')

//==============================================================================

Object.defineProperty(global, 'TEST_ROOT_PATH', {
  get: function () {
    return path.join(process.cwd(), 'test')
  },
})

//------------------------------------------------------------------------------

Object.defineProperty(global, 'TEST_HELPERS_PATH', {
  get: function () {
    return path.join(TEST_ROOT_PATH, 'helpers')
  },
})

//------------------------------------------------------------------------------

Object.defineProperty(global, 'TEST_FACTORIES_PATH', {
  get: function () {
    return path.join(TEST_ROOT_PATH, 'factories')
  },
})

//------------------------------------------------------------------------------

Object.defineProperty(global, 'TEST_FIXTURES_PATH', {
  get: function () {
    return path.join(TEST_ROOT_PATH, 'fixtures')
  },
})

//------------------------------------------------------------------------------
Object.defineProperty(global, 'TEST_NAME', {
  get: function () {
    var name = 'Anonymous'
    var caller = arguments.callee.caller.toString()
    var args = arguments.callee.caller.arguments
    var match = caller.match(/exports, *require, *module, *__filename, *__dirname/)

    if (match) {
      var filename = args[3] || name
      name = filename.replace(TEST_ROOT_PATH, '')
                     .replace(/^.?unit(\\|\/)?/, '')
                     .replace(/(\.(test|spec))?\.js$/, '')
    }
    return name
  },
})

//==============================================================================

let xhr = null
Object.defineProperty(global, 'xhr', {
  get: function () {
    if (!xhr) {
      let app = (sails.express) ? sails.express.app : sails.hooks.http.app
      xhr = require('supertest')(app)
    }
    return xhr
  },
})



let request = null
Object.defineProperty(global, 'request', {
  get: function () {
    if (!request) {
      let app = (sails.express) ? sails.express.app : sails.hooks.http.app
      request = new (require('supertest-session')({ app: app }))()
    }
    return request
  },
})



Object.defineProperty(global, 'sinon', {
  get: function () {
    return sinon
  },
})


Object.defineProperty(global, 'stub', {
  get: function () {
    return sinon.stub
  },
})

Object.defineProperty(global, 'mock', {
  get: function () {
    return sinon.mock
  },
})

Object.defineProperty(global, 'expect', {
  get: function () {
    return chai.expect
  },
})

Object.defineProperty(global, 'requireHelper', {
  get: function () {
    return requireHelper
  },
})

//==============================================================================
//-- locals

function requireHelper(module) {
  var _module = path.join(TEST_HELPERS_PATH, module)
  return require(_module)
}

