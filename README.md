# sails-tests-harness
Initialize an opinionated Mocha Framework Tests harness including a SailsJs unit-test file generator. Module uses a in-memory MongoDB to make sure tests are extremely fast.

### Installation

```sh
# Local install
$ npm install sails-tests-harness

# Global install
$ sudo npm install -g sails-tests-harness
```

Note that it will also download an in-memory MongoDB binary (about 50MB) during the build phase of the module installation, which may take a while.


## Initialization / Harnessing

 The initialization should be run in the root dir of the a SailsJs App that will be using this module. It will inject a SailsJs generator into .sailsrc to make it easier to create test files:
 
```sh
# When you used a local install earlier
$ npx sails-tests-harness init | harness

# When you used a global install earlier
$ sails-tests-harness init | harness
```

The following will be copied to the current working directory and will prompt to overwrite existing ones:

```sh
Sails App Root: 
└── Makefile                    # Make scripts to ease test runs
└── config
│   └── env
│       └── test.js             # test environment config
└── test
│   ├── factories               # factories directory
│   ├── fixtures                # fixtures directory
│   └── helpers
│       └── bootstrap.js        # custom bootstap file
│   └── unit                    # unit tests directory
│       ├── controllers
│       ├── helpers
│       ├── models
│       ├── services
│   └── integration             # Integration tests
│       └── app.test.js         # Entire app integration tests
│   └── mocha.opts              # Mocha configuration options
```

### Generating Tests

```sails generate test controller user```    
> will create test file: 'test/unit/controllers/UserController.test.js'

```sails generate test model user```
> will create test file: 'create test/unit/models/User.test.js'

```sails generate test service validation```
> will create test file: 'test/unit/services/Validation.test.js'

```sails generate test helper validation```
> will create test file: 'test/unit/helpers/Validation.test.js'



### Controllers


```javascript
//-- test/unit/controllers/SampleController.test.js
describe(TEST_NAME, function() {
  describe("GET index", function() {
    it("should be successful", function(done) {
      request.get("/sample")
        .expect(200)
        .end(done);
    });
  });
});
```


_Execute SampleController test_

```
$ make test

  controllers/SampleController
    GET index
      ✓ should be successful

  1 passing
```


### Models

```javascript
//-- test/unit/models/Sample.test.js
describe(TEST_NAME, function() {
  describe(".create()", function() {
    it("should be successful", function(done) {
      Sample.create().exec(function(err, record) {
        expect(err).to.not.exist;
        expect(record).to.exist;
        done();
      });
    });
  });
});
```


_Execute Sample test_

```
$ make test

  models/Sample
    .create()
      ✓ should be successful 

  1 passing
```

and so forth... you can do the same for services and helpers as well.

## Test Execution

Tests are executed using **_make_** command. Basically the script will look for tests to be executed in **test/unit/** directory.

```sh
# Run all tests
$ make test

# Run tests under a specific directory
# This will run all tests under test/unit/controllers directory
$ make test controllers

# This will run tests under test/unit/controllers and test/unit/models directories
$ make test controllers models

# Run a specific test file
# This will run tests in test/unit/controllers/SampleController.test.js file
$ make test controllers/SampleController.test
```

### Tests Options

Use **_TESTS_OPTS_** commandline variable to control the tests harness by passing options using the following syntax:

```sh
# don't spin in-memory MongoDB
$ make test TESTS_OPTS='--no-mongo'
```

##### Available options:
> **_--no-mongo_**: Doesn't spin the in-memory MongoDB, maybe you want to inspect test data later and don't want it to disappear into lala land. Note that you will have to have a running MongoDB or other configured in datastore for this to work. When you disable the fast Mongo memory DB, then the test env configuration will turn off other options.

> **_--no-sails_**: Skips loading of SailsJs. Why? If you have unit tests that don't require Sails app and run their tests without the instance, then isolating and testing those tests files can speed up testing them since Sails is just an unnecessary huddle.

> **_--no-boot_**: If for some reason you don't want to load the harness' test bootstrapper then this is where you disable it

### Mocha Options

Mocha options can be passed as parameter to **_make_**. You can actually execute Mocha directly and the harness will still boot-up and work. By default, **_mocha_** is being executed using the following options:

```sh
# recursive with 30 second timeout using spec reporter
$ mocha --recursive -t 30000 -R spec
```

Use **MOCHA_OPTS** commandline variable to pass specific **_mocha_** options to **_make_**.

```sh
# Dot format without colors. Useful for test execution on CI servers such as Jenkins. 
$ make MOCHA_OPTS='-C -R dot' test
```



## Helpers

Use global helpers in yours test suites:

##### Information variables
* TEST_NAME
* TEST_ROOT_PATH
* TEST_HELPERS_PATH
* TEST_FACTORIES_PATH
* TEST_FIXTURES_PATH

##### Global helpers
Use these directly in tests files as functions etc
* requireHelper()
* [sinon](https://github.com/cjohansen/Sinon.JS) - sinon
* [stub()](https://github.com/cjohansen/Sinon.JS) - sinon.stub
* [mock()](https://github.com/cjohansen/Sinon.JS) - sinon.mock
* [chai](https://github.com/chaijs/chai) - chai (use this to register other chai plugins)
* [expect()](https://github.com/chaijs/chai) - chai.expect
* [request](https://github.com/rjz/supertest-session) - supertest-session
* [xhr](https://github.com/visionmedia/supertest) - supertest
* [chalk](https://github.com/chalk/chalk) - chalk
* [faker](http://github.com/Marak/Faker) - faker
* [factory](https://github.com/chaijs/chai-factories) - chai-factories (more general factory function)
* [contrace](https://github.com/emahuni/contrace) - contrace - good logging tool
* [_](https://github.com/originate/chai-match-pattern) - chai-match-pattern => lodash-match-pattern

##### Dependencies

You can use the following dependencies, which are not straight globals. Read documentations to understand how to use them.

* [should](https://github.com/chaijs/chai) - chai.should
* [barrels](https://github.com/bredikhin/barrels) - barrels - Simple DB Fixtures for Sails.js with Associations Support


## Fixtures
[Barrels](https://github.com/bredikhin/barrels) is amazing (**_WIP Not for sails 1_** going to fix this), see the documentation. Drop your fixtures in './test/fixtures' as JSON files (or CommonJS modules) named after your models. eg: 
```js
// ./test/fixtures/products.js
[
  {
    "title": "Leather Jacket",
    "category": 1,
    "tags": [1,2,3],
    "seller": 1,
    "regions": [ 1,2]
  },
  // ... each obj here is a record
]
```
> Those numbers are ID's that will be matched against corresponding model fixtures.

Once the bootstrap of the SailsJs test app is ready those records will be available in the DB for query etc, automatically associated.


## Custom Helpers

You can write your own test helpers or node modules and save it under **test/helpers/** directory. Use the built-in **requireHelper()** function to load your custom helper.

```javascript
//-- test/unit/services/SampleService.test.js
require("sails-test-helper");

describe(TEST_NAME, function() {
  it("should load my custom helper", function() {
    let my_helper = requireHelper("my_helper");
    
    expect(my_helper).to.exist;
  });
});
```

If you need to do some initialization prior to all your tests execution, you can put them inside **test/helpers/bootstrap.js** file. This file will be loaded automatically upon test execution.

```javascript
//-- test/helpers/bootstrap.js
//-- global variables can also be initialized here...

before(function(done) {
  //-- anything to run or initialize before running all tests...
  
  done();
});
```


# Author
Emmanuel Mahuni

License MIT

# Attribution
Thanks to these guys:

[Sails-helper-file-generator](https://github.com/makmakulet/sails-test-helper-file-generator.git)

[Sails-Test-Helper](https://github.com/zand3rs/sails-test-helper)

## Todo
- Fix Barrels
- support controller actions test generation
- add fixtures generator based on model
- add more flesh to generated files
- make sure tests run properly (they are practically dead)
