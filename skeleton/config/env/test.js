/**
 * Load testing environment based on the testing harnessing from sails-tests-harness
 * You can customize this file to suite your needs
 */

// get app package information
const pkg = require('../../package.json')

module.exports = {
  // only change the datastore settings (from /config/datastore.js) if that env var is set
  datastores: process.env.MONGODB_URL && {
    default: {
      adapter: 'sails-mongo',
      url:     process.env.MONGODB_URL,
    },
  },

  models: {
    // put migration to safe so that we speed up things
    migrate: 'safe',

    // also set mongo attributes here if...
    attributes: process.env.MONGODB_URL && {
      // make sure we are using mongo specific model settings
      id: { type: 'string', columnName: '_id' },
    },
  },

}
