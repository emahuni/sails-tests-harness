#
# Makefile for mocha
# by: zander - zand3rs@gmail.com
#     and Emmanuel Mahuni
#
# usage:
# To run all tests -
# make test
#
# To run a particular test
# make test <path/to/test>
#
# To run a particular category of tests
# make test models
# make test controllers
# make test services
# make test helpers
#
# override default MOCHA_OPTS:
# make MOCHA_OPTS='-C -R dot' test
#

ENV_VARS=NODE_ENV=test PORT=9999
UNIT_TEST_DIR=test/unit/
INTEGRATION_TEST_DIR=test/integration/

MOCHA_BIN=mocha
MOCHA_DEFAULT_OPTS=--recursive -t 30000
MOCHA_OPTS=-R spec

ifneq "$(wildcard ./node_modules/sails-tests-harness/node_modules/.bin/mocha)" ""
    MOCHA_BIN=./node_modules/sails-tests-harness/node_modules/.bin/mocha
endif
ifneq "$(wildcard ./node_modules/.bin/mocha)" ""
    MOCHA_BIN=./node_modules/.bin/mocha
endif


check: test

test:
	@$(eval TARGETS=$(filter-out $@,$(MAKECMDGOALS)))
	@$(eval TARGETS=$(TARGETS:test/%=%))
	@$(eval TARGETS=$(TARGETS:unit%=%))
	@$(eval TARGETS=$(TARGETS:/%=%))
	@$(eval TARGETS=$(addprefix $(UNIT_TEST_DIR),$(TARGETS)))
	@$(eval TARGET=$(shell [ -z $(firstword ${TARGETS}) ] && echo ${INTEGRATION_TEST_DIR} ${UNIT_TEST_DIR} ))
	$(ENV_VARS) $(MOCHA_BIN) $(MOCHA_DEFAULT_OPTS) $(MOCHA_OPTS) $(TARGET) $(TARGETS) $(TESTS_OPTS)

silent:
	@:

%: silent
	@:

.PHONY: check silent test
