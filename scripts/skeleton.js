const fs = require('fs-extra')
const dirTree = require('directory-tree')
const tree = require('tre')
const path = require('path')
const chalk = require('chalk')

const { prompt } = require('enquirer')


module.exports = async function () {
  const skelPath = path.join(__dirname, '..', 'skeleton')
  let files = dirTree(skelPath)
// console.dir(files)
  const filesTree = tree(files.children, { root: 'Sails App Root: ' })

  console.info(chalk`{cyan
Harnessing tests framework. This will create/overwrite the tests directory and harness its setup files and helpers.
You will be prompted if any of these files exist and backup copies will be created for later review or restoration:
 
{yellow ${filesTree}}
 
 }`)

  async function copyFile(src, dest) {
    let doCopy = true, exists, answ, des = dest.replace(process.cwd(), '.')

    // the file exists and isn't a .gitkeep file (inferring that the dir already exists, no need to copy this)
    if ((exists = fs.existsSync(dest)) && (doCopy = !dest.includes('.gitkeep'))) {
      answ = await prompt({ type: 'toggle', name: 'overwrite', message: ` Overwrite "${des}"?`, initial: false })
        .catch(err => {console.warn('⚠️ Some error happened...')})

      if (answ.overwrite) {
        // console.log('Overwriting: ', des)
      } else {
        // console.log(`Don't overwrite`)
        doCopy = false
      }
      // console.debug('answ: ', answ)
    }

    if (doCopy) {
      if (exists) {
        console.info(chalk`{grey {bold Creating Backup:} ${des}.bck}`)
        fs.copySync(dest, `${dest}.bck`)
      }

      console.info(chalk`{grey {bold Copying to:} ${des}}
      `)
      fs.copySync(src, dest)
    }

    return answ
  }

  async function copy(src, dest = process.cwd()) {
    let state

    for (let s of src) {
      let d = path.join(dest, s.path.replace(skelPath, ''))

      // check to see if this is a file
      if (s.type === 'file') {
        state = await copyFile(s.path, d) // copy the file
      } else if (s.children) {
        state = await copy(s.children, dest) // traverse the children of the dir
      }
    }

    return state
  }

  await copy(files.children)
}
