const sailsRc = process.cwd() + '/.sailsrc'

// console.debug('sails rc file: ', sailsRc)

const fs = require('fs')
const chalk = require('chalk')

module.exports = async function () {
  if (!fs.existsSync(sailsRc)) {
    console.log(chalk`{red
Unable to install generator on the main APP. ".sailsrc" file not found, please create the .sailsrc file on the root path of your app
}`)
  } else {
    const data = fs.readFileSync(sailsRc, 'utf8')

    try {
      const sailsRcConfig = JSON.parse(data)
      const generators = sailsRcConfig.generators || {}
      const modules = generators.modules || {}
      modules['test'] = 'sails-tests-harness'
      generators.modules = modules
      sailsRcConfig.generators = generators

      const newConfig = JSON.stringify(sailsRcConfig, null, 2)

      fs.writeFileSync(sailsRc, newConfig)
      console.log(chalk`{white
Injected tests generator...
Updated .sailsrc new config:
 
{grey ${newConfig}}
}`)
    } catch (e) {
      console.error(`chalk{red
Error parsing / writing .sailsrc file 
${e}
}`)
    }
  }
}
