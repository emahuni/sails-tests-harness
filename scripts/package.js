const fs = require('fs-extra')
const path = require('path')
const chalk = require('chalk')

module.exports = function () {
  console.log(chalk`{white
  Creating "test" script in package.json (backs up any existing as "test-old")...
  }`)
  const pkgFile = path.resolve(process.cwd(), './package.json')
  const pkg = require(pkgFile)
  if (pkg.scripts) {
    if (pkg.scripts.hasOwnProperty('test')) {
      pkg.scripts['test-old'] = pkg.scripts.test
    }
  } else {
    pkg.scripts = {}
  }

  pkg.scripts.test = `make --makefile=${path.resolve(__dirname, '../Makefile')} test`

  fs.writeFileSync(pkgFile, JSON.stringify(pkg))
}
