#!/usr/bin/env node

const pkg = require('../package.json')


const program = require('commander')
const args = process.argv.slice(2)

program
  .version(pkg.version, '-v, --version')
  .usage('[command] [options]')

program
  .command('init')
  .description('Harness Test environment.')
  .action(init)

program.parse(process.argv)
!args.length && program.help()

async function init() {
  console.info(`
Initializing tests harness... 
  `)
  await require('../scripts/package')() // make sure that we have installed the generator
  await require('../scripts/sailsrc')() // make sure that we have installed the generator
  await require('../scripts/skeleton')() // harness tests

  console.info(`🔥 Done.`)
}
